<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    public function calcTaxes(Request $request) {
    	return response([
    		'valueWithTaxes' => ( $this->taxConvert($request->tax) * $request->amount ) + $request->amount
    	], 201);	
    }
    private function taxConvert($tax) {
    	return $tax / 100;
    }
}
